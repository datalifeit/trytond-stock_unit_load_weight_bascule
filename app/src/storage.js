var CONFIG = require('../config.json');

Storage.prototype.setObject = function(key, value) {
  this.setItem(key, JSON.stringify(value));
};
Storage.prototype.getObject = function(key) {
  var value = this.getItem(key);
  return value && JSON.parse(value);
};

var storage = {
  clear: function() {
    ['settings', 'employee'].forEach(function(name) {
      localStorage.removeItem(CONFIG.app_name + '_' + name);
    });
    ['employees', 'languages'].forEach(function(name) {
      sessionStorage.removeItem(CONFIG.app_name + '_' + name);
    });
  }
};


// setter local storage object values
['settings'].forEach(function(name){
  Object.defineProperty(storage, name, {
    get: function () { return localStorage.getObject(CONFIG.app_name + '_' + name) || {}; },
    set: function (value) {localStorage.setObject(CONFIG.app_name + '_' + name, value); },
    enumerable: true,
    configurable: true
  });
});

// settter local storage raw values
['language', 'employee'].forEach(function(name) {
  Object.defineProperty(storage, name, {
    get: function() { return localStorage.getItem(CONFIG.app_name + '_' + name); },
    set: function(value) { localStorage.setItem(CONFIG.app_name + '_' + name, value); },
    enumerable: true,
    configurable: true
  });
});

// setter local storage stringify values
['users_settings'].forEach(function (name) {
  Object.defineProperty(storage, name, {
    get: function(){
      return JSON.parse(localStorage.getItem(CONFIG.app_name + '_' + name));
    },
    set: function(value){
      value = JSON.stringify(value);
      localStorage.setItem(CONFIG.app_name + '_' + name, value);
    },
    enumerable: true,
    configurable: true
  });
});

// setter session storage object values
['employees', 'languages'].forEach(function(name) {
  Object.defineProperty(storage, name, {
    get: function() {
      return sessionStorage.getObject(CONFIG.app_name + name);
    },
    set: function(value) {
      sessionStorage.setObject(CONFIG.app_name + name, value);
    },
    enumerable: true,
    configurable: true
  });
});

// setter session storage bascule object
['bascule'].forEach(function(name) {
  Object.defineProperty(storage, name, {
    get: function() {
      return sessionStorage.getObject(CONFIG.app_name + name);
    },
    set: function(value) {
      sessionStorage.setObject(CONFIG.app_name + name, value);
    },
    enumerable: true,
    configurable: true
  });
});

// local storage defaults values
var localstorage_default_values = {
  'users_settings': []
};
Object.entries(localstorage_default_values)
  .forEach(function(key_value, index){
    if(storage[key_value[0]] != undefined){return};
    storage[key_value[0]] = key_value[1];
})


module.exports = storage;
