function debounce(func, wait) {
  return function() {
    var that = this,
      args = [].slice(arguments);

    clearTimeout(func._debounceTimeout);

    func._debounceTimeout = setTimeout(function() {
      func.apply(that, args);
    }, wait);
  };
}

function round(value, digits) {
  var precision = digits,
    factor;
  if (precision < 1) {
    var exp = -Math.floor(Math.log10(precision));
    factor = Math.pow(10, exp);
    value *= factor;
    precision *= factor;
  } else {
    factor = 1;
  }
  return Math.round(value / precision) * precision / factor;
}

function strip(html) {
  var tmp = document.createElement('DIV');
  tmp.innerHTML = html;
  return tmp.textContent || tmp.innerText || '';
}

function replaceClasses(el, newObject, oldObject) {
  if (oldObject) {
    var oldClasses = oldObject.classes || [];
    for (var oldClass of oldClasses) {
      el.classList.remove(oldClass);
    }
  }
  var newClasses = newObject.classes || [];
  for (var newClass of newClasses) {
    el.classList.add(newClass);
  }
}

function isEqualObject(a, b){
  var aProps = Object.getOwnPropertyNames(a);
  var bProps = Object.getOwnPropertyNames(b);

  if (aProps.length != bProps.length) {
    return false;
  }
  for (var i = 0; i < aProps.length; i++) {
    var propName = aProps[i];
    if (a[propName] !== b[propName]) {
      return false;
    }
  }
  return true;
}

module.exports = {
  debounce: debounce,
  round: round,
  replaceClasses: replaceClasses,
  isEqualObject: isEqualObject
};
