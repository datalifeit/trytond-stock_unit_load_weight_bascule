# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class UserApplication(metaclass=PoolMeta):
    __name__ = 'res.user.application'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.application.selection.append(('bascule_app', "Bascule App"))


class User(metaclass=PoolMeta):
    __name__ = 'res.user'

    def _get_bascule_app_context(self):
        return {'warehouse': self.warehouse and self.warehouse.id or None}
