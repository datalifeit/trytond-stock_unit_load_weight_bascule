# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import json
import logging
import os
from functools import wraps

from werkzeug import Response
from werkzeug.datastructures import Headers
from werkzeug.middleware.shared_data import SharedDataMiddleware

from trytond.config import config
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wsgi import app
from trytond.protocols.wrappers import (
    user_application, with_pool, with_transaction)

logger = logging.getLogger(__name__)
APP_DATE_FORMAT = '%Y-%m-%d'
DATABASE = config.get('bascule_app', 'database', default='tryton')
TIMEOUT = config.getint('bascule_app', 'session_timeout',
    default=(60 * 60 * 24 * 7))

with open(os.path.join(os.path.dirname(__file__), 'app', 'config.json')) as f:
    file_config = json.load(f)
    APP_NAME = file_config['app_name']


def url(database=True, endpoint=''):
    format_ = '/%s/%s'
    params = [APP_NAME, endpoint]
    if database:
        format_ += '/%s'
        params.insert(0, '<database_name>')
    return format_ % tuple(params)


def with_database(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        return func(request, DATABASE, *args, **kwargs)
    return wrapper


def custom_except_response(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        try:
            return func(request, *args, **kwargs)
        except UserError as e:
            text = str(e)
            Transaction().rollback()
            return Response(response=json.dumps({'text': text}), status=500)
        except Exception as e:
            raise e
            text = str(e)
            logger.error(text)
            Transaction().rollback()
            return Response(response=json.dumps({'text': text}), status=500)
    return wrapper


def get_employee(request):
    pool = Pool()
    Employee = pool.get('company.employee')
    try:
        employee = int(request.headers['X-Employee'])
    except (KeyError, ValueError):
        return None
    return Employee(employee)


@app.route(url(''), methods=['GET'])
def index(request):
    g = open(os.path.join(os.path.dirname(__file__), 'app', 'index.html'))
    return Response(g.read(), mimetype='text/html', direct_passthrough=True)


def application(func):
    @wraps(func)
    @user_application(APP_NAME)
    @custom_except_response
    def wrapper(request, *args, **kwargs):
        pool = Pool()
        User = pool.get('res.user')

        ctx = {
            'company': None,
            'employee': None,
            }
        employee = get_employee(request)
        user = User(Transaction().user)
        if employee:
            ctx['company'] = employee.company.id
            ctx['employee'] = employee.id
        else:
            company = user.company or user.main_company
            if company:
                ctx['company'] = company.id
        ctx.update(user._get_bascule_app_context())
        with Transaction().set_context(ctx):
            return func(request, *args, **kwargs)
    return wrapper


@app.route(url(endpoint='weight_ul'), methods=['PUT'])
@with_pool
@with_transaction()
@application
def weight_ul(request, pool):
    pool = Pool()
    UnitLoad = pool.get('stock.unit_load')

    data = request.parsed_data.copy()
    ul_code = data.get('ul_code')
    gross_weight = data.get('gross_weight')
    ul_domain = UnitLoad._get_barcode_search_domain(ul_code)
    ul, = UnitLoad.search([ul_domain]) or [None]
    if not ul:
        raise UserError(gettext(
            'stock_unit_load_weight_bascule.'
            'msg_app_ul_not_found',
            unit_load=ul_code))
    new_gross_weight = float(gross_weight)
    if ul.gross_weight != new_gross_weight:
        ul.gross_weight = new_gross_weight
        ul.on_change_gross_weight()
        ul.save()

    return {
        'unit_load': ul.code,
        'gross_weight': ul.gross_weight,
        'net_weight': ul.net_weight,
        'tare': ul.tare
    }


class CORSMiddleware(object):
    """Add Cross-origin resource sharing headers to every request."""

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):

        def add_cors_headers(status, headers, exc_info=None):
            headers = Headers(headers)
            headers.add('Access-Control-Allow-Origin', '*')
            headers.add('Access-Control-Allow-Headers',
                'Origin, content-type, authorization, x-employee')
            headers.add('Access-Control-Allow-Credentials', 'true')
            headers.add('Access-Control-Allow-Methods',
                'GET, POST, PUT, DELETE')

            return start_response(status, headers.to_wsgi_list(), exc_info)

        if environ.get('REQUEST_METHOD') == 'OPTIONS':
            add_cors_headers('200 Ok', [('Content-Type', 'text/plain')])
            return [b'200 Ok']

        return self.app(environ, add_cors_headers)


app.wsgi_app = CORSMiddleware(app.wsgi_app)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app,
    {'/%s/static' % APP_NAME: os.path.join(os.path.dirname(__file__), 'app')})
