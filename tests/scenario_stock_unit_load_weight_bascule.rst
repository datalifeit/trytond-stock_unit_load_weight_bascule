=======================================
Stock Unit Load Weight Bascule Scenario
=======================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from unittest.mock import patch
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)

Install stock_unit_load_weight_bascule::

    >>> config = activate_modules('stock_unit_load_weight_bascule')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product1 = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product1.template = template
    >>> product1.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])

Create an unit load with product in kilograms::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.warehouse = warehouse_loc
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = production_loc
    >>> unit_load.product = product1
    >>> unit_load.cases_quantity = 5
    >>> unit_load.save()
    >>> unit_load.quantity = Decimal('40.0')
    >>> unit_load.tare = Decimal('20.0')
    >>> unit_load.gross_weight = Decimal('450.0')
    >>> unit_load.save()

Configure a bascule for current user::

    >>> Bascule = Model.get('stock.bascule')
    >>> Irmodel = Model.get('ir.model')
    >>> User = Model.get('res.user')
    >>> model, = Irmodel.find([('model', '=', 'stock.unit_load')])
    >>> bascule = Bascule(name='Bascule 1', model=model,
    ...     uri='tcp://localhost:6767')
    >>> bascule.save()
    >>> user = User(config.user)
    >>> user_bascule = user.bascules.new()
    >>> user_bascule.bascule = bascule
    >>> user.save()

Mock reading measure and weigh::

    >>> with patch('trytond.modules.stock_bascule.bascule.socket.socket'
    ...         ) as mock_socket:
    ...     mock_socket().__enter__().recv().decode.return_value = '467.0'
    ...     weigh_unit_load = Wizard('stock.unit_load.weigh', [unit_load])
    ...     weigh_unit_load.form.gross_weight == 467.0
    ...     weigh_unit_load.form.gross_weight = 480.0
    ...     weigh_unit_load.execute('data')
    ...     weigh_unit_load.form.gross_weight == 467.0
    True
    True